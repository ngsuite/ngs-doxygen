# NGSuite Documentation

This is a joint [Doxygen documentation for the following projects](https://ngsuite.pages.gwdg.de/ngs-doxygen/):

- [NGSolve](https://github.com/NGSolve/ngsolve)
- [NGSTrefftz](https://github.com/PaulSt/NGSTrefftz)
- [ngsxfem](https://github.com/ngsxfem/ngsxfem)

## Licenses

The projects fall under the following licenses:

- [NGSolve: LGPLv2](https://github.com/NGSolve/ngsolve/blob/master/LICENSE)
- [NGSTrefftz: LGPLv3](https://github.com/PaulSt/NGSTrefftz/blob/main/LICENSE)
- [ngsxfem: LGPLv3](https://github.com/ngsxfem/ngsxfem/blob/master/LICENSE)

## Doxygen Theme

This documentation uses the
[Doxygen Awesome](https://github.com/jothepro/doxygen-awesome-css) theme.
